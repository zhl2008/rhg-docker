#!/bin/bash

sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
sed -i "s/secure_file_priv='\/'/secure_file_priv=\/bin/g" /etc/mysql/mysql.cnf
service mysql stop
service mysql start
service ssh start
service apache2 start
mysql -uroot -proot -e "grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;"&&\
mysql -uroot -proot -e "flush privileges;"&&\
mysql -uroot -proot < /root/wordpress.sql

cp /root/flag /tmp/flag
echo root:root|chpasswd
tail -f /dev/null
