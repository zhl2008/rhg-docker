#!/bin/bash

/root/clean.sh

service apache2 start
service ssh start
cp /root/flag /tmp/flag
cd /var/www/html
tar xvfz git.tgz
sed -i 's/172.16.50.3:3306/172.16.50.3:3306/g' /var/www/html/wp-config.php
tail -f /dev/null
