#!/bin/bash

/root/clean.sh

# startt mysql
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf
service mysql stop
service mysql start
mysql -uroot -proot -e "drop database if exists wordpress;"&&\
mysql -uroot -proot -e "create database wordpress;"&&\
mysql -uroot -proot -e "grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;"&&\
mysql -uroot -proot -e "flush privileges;"&&\
mysql -uroot -proot  < /root/wordpress.sql

service apache2 start
service ssh start
cp /root/flag /tmp/flag
sed -i 's/172.16.10.3:3306/127.0.0.1:3306/g' /var/www/html/wp-config.php
chmod 755 -R /var/log/apache2

tail -f /dev/null
