#!/bin/bash

sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

killall java
chown root:tomcat7 -R /etc/tomcat7/
service tomcat7 start
service ssh start

cp /root/flag /tmp/flag
echo root:root|chpasswd
tail -f /dev/null
