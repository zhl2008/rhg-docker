<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

define('WPLANG', 'zh_CN');
/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '172.16.10.3:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ar$HF(euVKdRH<yCL-bq$HUzmI[1l7jOj}83Ckb!5^z/?tbUiNu$,W]+z8?6_Drc');
define('SECURE_AUTH_KEY',  'Mzv64pf*iotc6Kexf~3Sxl!5]|S6pcRBDo@[-K7,Ac]J{CEh4BYA^bXbKGP6+G<=');
define('LOGGED_IN_KEY',    'v,0NA>a<;&=>:+u25X6vxL7Gj.@-GR$gXO~t/D(shxRNEJfF5Xo`JkVwkd Tr,. ');
define('NONCE_KEY',        'x;J5E|RPwDX|Ul:OcaNDGGtvjP F7dI:^ 4 Ua|ft:RqR#].hCE%l3pk1k{F[rTE');
define('AUTH_SALT',        'Kz&6K,BUD<;Gz}@[je1tb #SF?nkH`b:IIZhN4$m$Y @_K-ZQ^6#ywM7,+ Jp ~P');
define('SECURE_AUTH_SALT', 'G3csdfc.+ZR}&vbCqE8>S=F!WY*VXs81%HZ*kMQ]ysP)>X;4SyRpycso0}M(@cWE');
define('LOGGED_IN_SALT',   'CC]xp3d?XoMXlAh-1u+12(M;43Y$+WRK.p-@R8$[2QMa(~Dvkj!xm5+WK(_U;^FM');
define('NONCE_SALT',       '[S[*%oZ{[^7*<szvxoUv+qVwOXfZ+Bb%17z_ezxV~ECQzy8-nuCRTYG^{2(JMhs^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
