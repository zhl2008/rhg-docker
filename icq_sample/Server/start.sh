#!/bin/bash

chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
service mysql start
service ssh start
mysql -uroot -proot -e "drop database if exists wordpress;"&&\
mysql -uroot -proot -e "create database wordpress;"&&\
mysql -uroot -proot -e "grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;"&&\
mysql -uroot -proot -e "flush privileges;"&&\
mysql -uroot -proot wordpress < /root/wordpress.sql

cp /root/flag /tmp/flag

tail -f /dev/null
