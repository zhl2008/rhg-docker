#!/usr/bin/env python

import os
import sys

compose_dirs = ['1','2','3','4','5','6','7','8','9','a']
compose_dirs = ['icq_' + x for x in compose_dirs]


def start():
    for compose_dir in compose_dirs:
	os.chdir(compose_dir)
	os.system('docker-compose up -d')
	os.chdir('../')

def stop():
    for compose_dir in compose_dirs:
	os.chdir(compose_dir)
	os.system('docker-compose down')
	os.chdir("../")

if __name__ == '__main__':
    action = sys.argv[1]
    if action == 'start':
	start()
    elif action == 'stop':
	stop()
    else:
	print 'no such action'
