<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/;oni+1^nbMi}kkP5RCq1$F<lkswvU3f%?93q,V/P}J1X~p++=Ej(w;qm2+an729');
define('SECURE_AUTH_KEY',  'J<O](L]!jVcPPWh%Gdg@qLCk(oW/;>z.S[WY%G@q&E>Q1K_=XCE;9ER??0;D%,YN');
define('LOGGED_IN_KEY',    '9Mzxrk23D|MwWs%(rg#2DG0bVMXL_,n*{IlLOx~%pOv*j;6ZnI{K/0)#6x`J2p1b');
define('NONCE_KEY',        'gl6En<*>)hgWv(7J,rmr2V<9jMGq^/+22=!ZZxl|xE1K)_p}#OMLLT?Ly!1 ,n W');
define('AUTH_SALT',        'AcNsUw<UqvcC<{+CC+peG 7x-;ZN0y>%;K*uaoIzC7 LnOE[RWE2{^~L Zs|5PKO');
define('SECURE_AUTH_SALT', 'ggdRQGEG/My8z,G}tCG9ktn]F5>n=H}4!=!VZ=.y}||{<Ki|m`Sw~[!))B>KPlq#');
define('LOGGED_IN_SALT',   'ivG7Eg>}Tp9-KR7.QFSROb[i=YUfx_^2g Rt4(YJ(&=JyK2.VP]=>yzq<5~C^k(E');
define('NONCE_SALT',       '5@`C+fRi5==bhc`A^i6Z@G#D0H6;W*D6-jeR.7/OL2%d;zf)r-=[1BOc_Kz=S%pI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
