#!/bin/bash

/root/clean.sh

echo root:root|chpasswd
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

service mysql start
service apache2 start
service ssh start

mysql -uroot -proot -e "grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;"&&\
mysql -uroot -proot -e "flush privileges;"&&\
mysql -uroot -proot < /root/wordpress.sql


cp /root/flag /tmp/flag
tail -f /dev/null


