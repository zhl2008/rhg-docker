#!/bin/bash

/root/clean.sh

chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
service mysql start
service apache2 start
service ssh start

mysql -uroot -proot -e "grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;"&&\
mysql -uroot -proot -e "flush privileges;"&&\
mysql -uroot -proot < /root/wordpress.sql

cp /root/flag /tmp/flag
sed -i 's/localhost:3306/172.16.90.2:3306/g' /var/www/html/wp-config.php
tail -f /dev/null


