<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-X7:FTbNF2F8Bh`&NT{_Od?j@yP eobUcAFvUfktYN$$kWz?+nh{~jfD,0w!<#KQ' );
define( 'SECURE_AUTH_KEY',  '=FBP~1~0~4grDNi[na*/k@;evSwc:L`rr{Vxl?E!I^6jn=5b;?S%cW![@WD=<n@V' );
define( 'LOGGED_IN_KEY',    '8(xYVjQnLiZZVjsB]&]VFl60c||J5z4=)d]KzaCmqOHkBE*K8,9ms#?={ C=Q$M,' );
define( 'NONCE_KEY',        'qq7TfRMj&7*f8a7Pvq+fYtEYx?->(BLo:G20>DogH}p18|r)><G+#/uZ?|5gsfV7' );
define( 'AUTH_SALT',        'b?=8hgQ;Om[_wgNBGB}~vi]sDkt7k^LUv}!kt^~-XqUfeNjBxbMHR]]iR4lnch;C' );
define( 'SECURE_AUTH_SALT', '-;^$Y)j o&;`F3~A+BzfN}aX+p;r`h+m3(s7D$Q.s;?K}i!_9!I*=4:;jYK9MCaj' );
define( 'LOGGED_IN_SALT',   'QW^1L_?}>dV}OPyo.?}V,ESY8 [6Ql44l!aju-K0k)zdbkG^:KO]F/@z~6Yy*DRO' );
define( 'NONCE_SALT',       '?B!iUDayp;O:;Mu@O%!!edp9PXDOb?3qTcN <Y=fJiP-U)U><>Jz)_D7jBKHri2m' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
