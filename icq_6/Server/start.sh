#!/bin/bash

sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
/usr/local/samba/sbin/smbd -FS > /dev/null &
service ssh start&&\
cp /root/flag /tmp/
echo root:root|chpasswd
tail -f /dev/null
