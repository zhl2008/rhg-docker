#!/bin/bash

sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
service ssh start
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld

cp /root/flag /tmp/flag
echo root:root|chpasswd
tail -f /dev/null
