#!/bin/bash

# startt mysql
/root/clean.sh
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
service mysql start
mysql -uroot -proot -e "drop database if exists wordpress;"&&\
mysql -uroot -proot -e "create database wordpress;"&&\
mysql -uroot -proot -e "grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;"&&\
mysql -uroot -proot -e "flush privileges;"&&\
mysql -uroot -proot  < /root/wordpress.sql

service apache2 start
service ssh start
cp /root/flag /tmp/flag
cp /root/id_rsa /var/www/id_rsa
chmod 777 /var/www/id_rsa
sed -i 's/172.16.10.3:3306/127.0.0.1:3306/g' /var/www/html/wp-config.php
tail -f /dev/null
