#!/bin/bash

sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
service ssh start
service apache2 start

chmod +x /var/www/html/victim.cgi
cp /root/flag /tmp/flag
echo root:root|chpasswd
tail -f /dev/null
