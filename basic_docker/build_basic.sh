#!/bin/sh

docker build -t icq_web_demo ./web
docker build -t icq_server_demo ./server
docker build -t icq_together_demo ./together
