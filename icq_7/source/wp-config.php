<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RzrCq3q9@1 mvlPPHU$ZliCS6vwoMuRTU=_SgFgt`:@AO3is1Qn+L`@myjkHY!jB' );
define( 'SECURE_AUTH_KEY',  '.7TB9Hn[CY)[D@n-i=!u}x3^k&qH! egt6!kkWjD^ :P!K_|J7Zq<dDWXUKa<[,V' );
define( 'LOGGED_IN_KEY',    'nuTn. zpEY1*Ps;&OgV$b/X5cyHO[f%u1H}Jgn+0?n{py1J46Yv7?bs]j7pQoYY]' );
define( 'NONCE_KEY',        '.nd#%<Iwpz&:e}O-AoM4n:$8Io .J_Iu7!XMoO0>&4l9d`y-`keW`#M!,#f$*2Fw' );
define( 'AUTH_SALT',        'v5U])w!P=y,V}u+!57I%K1oIq}j6Kl#M9(dkiYWS?k.yr6V0}$P;H]4WF6/}H^`L' );
define( 'SECURE_AUTH_SALT', 'qVoSqx(VE6rFPCQjbSwd-0e;^ H+z=n6)-mU$jI3%9!?AAu[!{pZSKtFDXzPPPTP' );
define( 'LOGGED_IN_SALT',   'TL*oTl4H+RoGgq5%Q`^)|1_if#K9/3kw4KU+zEF+d$~j^2Wa??2R/DO[nZ+o7xHq' );
define( 'NONCE_SALT',       'U=koGQ_Tj,@.8l:$VNMR<Oc1WVlbV5C,3@8Z}s>90l.?3dg618i?ITrSYXNvAq-+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
